<?php

/**
 * @file
 * Admin UI functions.
 */

/**
 * Implements hook_form().
 * Main configuration form for Password Interceptor.
 */
function password_interceptor_admin_configuration() {
	$form = array();

	$form['password_interceptor_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Intercept Passwords'),
		'#description' => t('Switch on interception of passwords'),
		'#default_value' => variable_get('password_interceptor_enabled', 0),
	);

	return system_settings_form($form);
}
